# README.md

about learning RSeQC

## install RSeQC

- via python 3.4 pip(py 2.7 donot have pip at present)

`python3 -m pip install RSeQC` / `pip install RSeQC`

```bash
Collecting RSeQC
  Downloading RSeQC-2.6.4.tar.gz (141kB)
    100% |█████████████████| 143kB 16kB/s
    Complete output from command python setup.py egg_info:
    Traceback (most recent call last):
      File "<string>", line 20, in <module>
      File "/tmp/pip-build-61cy2nom/RSeQC/setup.py", line 11, in <module>
        print >> sys.stderr, "ERROR: RSeQC requires Python 2.7"
    TypeError: unsupported operand type(s) for >>: 'builtin_function_or_method' and '_io.TextIOWrapper'

    ----------------------------------------
Command "python setup.py egg_info" failed with error code 1 in /tmp/pip-build-61cy2nom/RSeQC
You are using pip version 7.1.2, however version 9.0.1 is available.
You should consider upgrading via the 'pip install --upgrade pip' command.
```

- noticed `RSeQC requires Python 2.7`, so install pip for py 2.7

refer to `learnskills/python/README.md`

- try to install RSeQC again
    - complie error and it told `lib/bx/bitset.c:4:20: fatal error: Python.h: 没有那个文件或目录`
    - cannot find the direct solution

- noticed the info on [pip - Docs / Installation](https://pip.pypa.io/en/stable/installing/)

`pip is already installed if you're using Python 2 >=2.7.9 or Python 3 >=3.4 binaries downloaded from python.org`

- try to re-install python 2.7

refer to `learnskills/python/README.md`

- install RSeQC again and success

```bash
sudo pip install RSeQC
```

```bash
Collecting RSeQC
  Using cached RSeQC-2.6.4-py2-none-any.whl
Requirement already satisfied: pysam in /usr/lib64/python2.7/site-packages (from RSeQC)
Collecting bx-python (from RSeQC)
  Using cached bx-python-0.7.3.tar.gz
Collecting cython>=0.17 (from RSeQC)
  Using cached Cython-0.27.3-cp27-cp27mu-manylinux1_x86_64.whl
Collecting numpy (from RSeQC)
  Downloading numpy-1.13.3-cp27-cp27mu-manylinux1_x86_64.whl (16.6MB)
    100% |████████████████████████████████| 16.7MB 62kB/s
Building wheels for collected packages: bx-python
  Running setup.py bdist_wheel for bx-python ... done
  Stored in directory: /root/.cache/pip/wheels/df/b1/42/a7c58fa8136c445101438be9556ab1033942eef66046567aab
Successfully built bx-python
Installing collected packages: bx-python, cython, numpy, RSeQC
Successfully installed RSeQC-2.6.4 bx-python-0.7.3 cython-0.27.3 numpy-1.13.3
```
