# learning git

## intro

### Distributed Version Control System

- Git
- Mercurial

### install Git and configure

`sudo zypper install git`

`git config --global user.name "mapleaf_soar"`

`git config --global user.email "aptx7842069@gmail.com"`

### creat a repository

- Creates a new local repository with the specified name

`git init [project-name]`

- Snapshots the file in preparation for versioning

`git add [file]`

- Records file snapshots permanently in version history

`git commit -m "[descriptive message]"`

### check status

- Lists all new or modified files to be committed

`git status`

- Shows file differences not yet staged

`git diff`

### Version roll back and forward

- Lists version history for the current branch

`git log` / `git log --pretty=oneline`

- Lists all version history

`git reflog`

- Discards all history and changes back to the specified commit

`git reset --hard commit_id|HEAD^[^]...`

### delete file and roll back

- Deletes the file from the working directory and stages the deletion

`git rm <file>`

- unstage

`git reset HEAD <file>`

- discard changes in working directory)

`git checkout -- <file>`

### exercise

```bash
cd /home/chengshen/software/Git/learngit

chengshen@linux-0ng9:~/software/Git/learngit> git init
Initialized empty Git repository in /home/chengshen/software/

chengshen@linux-0ng9:~/software/Git/learngit> git add learning_git.md

chengshen@linux-0ng9:~/software/Git/learngit> git commit -m "add learning_git.md including creat a repository"
[master (root-commit) 98027e6] add learning_git.md including creat a repository
 1 file changed, 17 insertions(+)
 create mode 100644 learning_git.md

chengshen@linux-0ng9:~/software/Git/learngit> git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   learning_git.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        .directory

no changes added to commit (use "git add" and/or "git commit -a")

chengshen@linux-0ng9:~/software/Git/learngit> git diff learning_git.md
diff --git a/learning_git.md b/learning_git.md
index 2493a48..d3196d5 100644
--- a/learning_git.md
+++ b/learning_git.md
@@ -11,7 +11,27 @@

 sudo zypper install git

+git config --global user.name "mapleaf_soar"
+
+git config --global user.email "aptx7842069@gmail.com"
+
 cd /home/chengshen/software/Git/learngit

-git init
+### creat a repository
+
+Creates a new local repository with the specified name
+
+git init [project-name]
+
+Snapshots the file in preparation for versioning
+
+git add [file]
lines 1-24
diff --git a/learning_git.md b/learning_git.md
```

press `Enter` to read rest info and `Backspace` to read back(use `up` and `down` button)

```bash
+
+Records file snapshots permanently in version history
+
+git commit -m "[descriptive message]"
+
+[master (root-commit) 98027e6] add learning_git.md including creat a repository
+ 1 file changed, 17 insertions(+)
+ create mode 100644 learning_git.md

lines 10-33/33 (END)
```

type `q` to quit `diff`

```bash
chengshen@linux-0ng9:~/software/Git/learngit> git add learning_git.md
chengshen@linux-0ng9:~/software/Git/learngit> git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        modified:   learning_git.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        .directory

chengshen@linux-0ng9:~/software/Git/learngit> git commit -m "add git diff"
[master a4ac898] add git diff
 1 file changed, 64 insertions(+), 1 deletion(-)
chengshen@linux-0ng9:~/software/Git/learngit> git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   learning_git.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        .directory

no changes added to commit (use "git add" and/or "git commit -a")

chengshen@linux-0ng9:~/software/Git/learngit> git add .directory
chengshen@linux-0ng9:~/software/Git/learngit> git commit -m "add .directory"
[master ddc5d3c] add .directory
 1 file changed, 6 insertions(+)
 create mode 100644 .directory

chengshen@linux-0ng9:~/software/Git/learngit> git status
On branch master
nothing to commit, working tree clean
```

#### example - delete file in disk / `git rm` / `git commit` / version back(deleted file comes back)

- delete file in Dolphin

```bash
chengshen@linux-0ng9:~/software/Git/learngit> git status
On branch master
Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        deleted:    Untitled-1

no changes added to commit (use "git add" and/or "git commit -a")
```

- `git rm`

```bash
chengshen@linux-0ng9:~/software/Git/learngit> git rm Untitled-1
rm 'Untitled-1'

chengshen@linux-0ng9:~/software/Git/learngit> git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        deleted:    Untitled-1
```

- `git commit`

```bash
chengshen@linux-0ng9:~/software/Git/learngit> git commit -m "remover Untitled-1"
[master 9fe1017] remover Untitled-1
 1 file changed, 1 deletion(-)
 delete mode 100644 Untitled-1

chengshen@linux-0ng9:~/software/Git/learngit> git status
On branch master
nothing to commit, working tree clean
```

- `git log`

```bash
chengshen@linux-0ng9:~/software/Git/learngit> git log

commit 9fe1017146df08b15142e7c091765e9d5184ccfd
Author: mapleaf_soar <aptx7842069@gmail.com>
Date:   Thu Nov 30 19:30:01 2017 +0800

    remover Untitled-1

commit 3bb97d8a2fdda94908dff4459f2dfd9eefe0ca0a
Author: mapleaf_soar <aptx7842069@gmail.com>
Date:   Thu Nov 30 19:04:35 2017 +0800

    add Untitled-1
```

- `git reset --hard HEAD^`

```bash
chengshen@linux-0ng9:~/software/Git/learngit> git reset --hard HEAD^
HEAD is now at 3bb97d8 add Untitled-1
```

- the removed filed came back

- check log

```bash
chengshen@linux-0ng9:~/software/Git/learngit> git log

commit 3bb97d8a2fdda94908dff4459f2dfd9eefe0ca0a
Author: mapleaf_soar <aptx7842069@gmail.com>
Date:   Thu Nov 30 19:04:35 2017 +0800

    add Untitled-1
```

#### example - delete file in disk / `git rm` / `git reset HEAD <file>` / `git checkout`(deleted file comes back)

- delete file in Dolphin

- `git rm`

```bash
chengshen@linux-0ng9:~/software/Git/learngit> git rm Untitled-1
rm 'Untitled-1'

chengshen@linux-0ng9:~/software/Git/learngit> git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        deleted:    Untitled-1
```

- `git reset HEAD <file>`

```bash
chengshen@linux-0ng9:~/software/Git/learngit> git reset HEAD Untitled-1
Unstaged changes after reset:
D       Untitled-1

chengshen@linux-0ng9:~/software/Git/learngit> git status
On branch master
Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        deleted:    Untitled-1

no changes added to commit (use "git add" and/or "git commit -a")
```

- `git checkout -- <file>`

```bash
chengshen@linux-0ng9:~/software/Git/learngit> git checkout -- Untitled-1

chengshen@linux-0ng9:~/software/Git/learngit> git status
On branch master
nothing to commit, working tree clean
```

- the removed filed came back
