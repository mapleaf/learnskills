# README.md

about learning Python

## openSUSE

- run python 2.7 by type `python`

- run python 3.4 by type `python3`

- upgrade pip need root privilege

sudo pip install --upgrade pip

- python 2.7 donot have pip, py 3.4 do

```bash
python -m pip install --upgrade pip
/usr/bin/python: No module named pip

pip -V
pip 7.1.2 from /usr/lib/python3.4/site-packages (python 3.4)
```

- install pip for py 2.7 with `get-pip.py`
    - securely download `get-pip.py`

    ```bash
    wget https://bootstrap.pypa.io/get-pip.py
    --2017-12-04 13:13:06--  https://bootstrap.pypa.io/get-pip.py
    正在解析主机 bootstrap.pypa.io (bootstrap.pypa.io)... 151.101.0.175, 151.101.64.175, 151.101.128.175, ...
    正在连接 bootstrap.pypa.io (bootstrap.pypa.io)|151.101.0.175|:443... 已连接。
    已发出 HTTP 请求，正在等待回应... 200 OK
    长度：1595408 (1.5M) [text/x-python]
    正在保存至: “get-pip.py”

    100%[===============================>] 1,595,408    171KB/s 用时 6.3s

    2017-12-04 13:13:13 (248 KB/s) - 已保存 “get-pip.py” [1595408/1595408])
    ```
    - run `get-pip.py` with root privilege

    ```bash
    sudo python get-pip.py
    Collecting pip
    Downloading pip-9.0.1-py2.py3-none-any.whl (1.3MB)
        100% |██████████████| 1.3MB 75kB/s
    Collecting setuptools
    Downloading setuptools-38.2.3-py2.py3-none-any.whl (489kB)
        100% |██████████████| 491kB 82kB/s
    Collecting wheel
    Downloading wheel-0.30.0-py2.py3-none-any.whl (49kB)
        100% |██████████████| 51kB 87kB/s
    Installing collected packages: pip, setuptools, wheel
    Successfully installed pip-9.0.1 setuptools-38.2.3 wheel-0.30.0
    ```
- after installing pip for py 2.7, command `pip` points pip of py 2.7

```bash
pip -V
pip 9.0.1 from /usr/lib/python2.7/site-packages (python 2.7)
```

- remove python 2.7

```bash
chengshen@linux-0ng9:~/development/learnskills/python> sudo zypper remove python
Loading repository data...
Reading installed packages...
Resolving package dependencies...

The following 49 packages are going to be REMOVED:
  command-not-found dbus-1-python gimp-plugins-python ibus ibus-branding-openSUSE-KDE ibus-googlepinyin ibus-gtk ibus-gtk3 ibus-lang ibus-libpinyin
  ibus-m17n ibus-pinyin ibus-qt ibus-sunpinyin ibus-table ibus-table-chinese-erbi ibus-table-chinese-stroke5 ibus-table-chinese-wu
  ibus-table-chinese-wubi-haifeng ibus-table-chinese-wubi-jidian ibus-table-chinese-yong libpurple libpurple-branding-openSUSE libpurple-lang
  libpurple-tcl PackageKit PackageKit-backend-zypp PackageKit-branding-openSUSE PackageKit-lang python python-configobj python-cryptography
  python-decorator python-gobject python-gobject2 python-gobject-cairo python-gobject-Gdk python-gtk python-ibus python-numpy python-pyOpenSSL
  python-six python-solv rpm-python scout snapper-zypp-plugin telepathy-haze tuned xorg-x11-Xvnc

49 packages to remove.
After the operation, 80.3 MiB will be freed.

Continue? [y/n/...? shows all options] (y): y
( 1/49) Removing PackageKit-lang-1.1.3-2.3.noarch ...................................................[done]
( 2/49) Removing command-not-found-0.1.0+20151023-2.1.noarch ........................................[done]
( 3/49) Removing gimp-plugins-python-2.8.18-2.3.1.x86_64 ............................................[done]
( 4/49) Removing ibus-branding-openSUSE-KDE-1.5.13-2.3.1.noarch .....................................[done]
( 5/49) Removing ibus-googlepinyin-0.1.2-12.2.noarch ................................................[done]
( 6/49) Removing ibus-gtk-1.5.13-2.3.1.x86_64 .......................................................[done]
( 7/49) Removing ibus-gtk3-1.5.13-2.3.1.x86_64 ......................................................[done]
( 8/49) Removing ibus-lang-1.5.13-2.3.1.noarch ......................................................[done]
( 9/49) Removing ibus-libpinyin-1.7.92-1.1.x86_64 ...................................................[done]
(10/49) Removing ibus-m17n-1.3.4-14.3.x86_64 ........................................................[done]
(11/49) Removing ibus-pinyin-1.5.0-13.3.1.x86_64 ....................................................[done]
(12/49) Removing ibus-qt-1.3.3-4.4.x86_64 ...........................................................[done]
(13/49) Removing ibus-sunpinyin-2.0.3-3.3.x86_64 ....................................................[done]
(14/49) Removing ibus-table-chinese-erbi-1.8.2-4.43.noarch ..........................................[done]
(15/49) Removing ibus-table-chinese-stroke5-1.8.2-4.43.noarch .......................................[done]
(16/49) Removing ibus-table-chinese-wu-1.8.2-4.43.noarch ............................................[done]
(17/49) Removing ibus-table-chinese-wubi-haifeng-1.8.2-4.43.noarch ..................................[done]
(18/49) Removing ibus-table-chinese-wubi-jidian-1.8.2-4.43.noarch ...................................[done]
(19/49) Removing ibus-table-chinese-yong-1.8.2-4.43.noarch ..........................................[done]
(20/49) Removing libpurple-lang-2.12.0-8.6.1.noarch .................................................[done]
(21/49) Removing libpurple-tcl-2.12.0-8.6.1.x86_64 ..................................................[done]
(22/49) Removing python-gobject-Gdk-3.20.1-3.2.x86_64 ...............................................[done]
(23/49) Removing snapper-zypp-plugin-0.3.3-2.2.noarch ...............................................[done]
(24/49) Removing telepathy-haze-0.8.0-5.2.x86_64 ....................................................[done]
(25/49) Removing tuned-2.4.1-5.3.noarch .............................................................[done]
(26/49) Removing xorg-x11-Xvnc-1.6.0-16.17.1.x86_64 .................................................[done]
(27/49) Removing PackageKit-branding-openSUSE-42.1-2.3.noarch .......................................[done]
(28/49) Removing scout-0.1.0+20151023-2.1.noarch ....................................................[done]
(29/49) Removing rpm-python-4.11.2-10.1.x86_64 ......................................................[done]
(30/49) Removing python-gtk-2.24.0-16.6.x86_64 ......................................................[done]
(31/49) Removing python-ibus-1.5.13-2.3.1.x86_64 ....................................................[done]
(32/49) Removing ibus-table-1.8.11-4.7.x86_64 .......................................................[done]
(33/49) Removing python-gobject-cairo-3.20.1-3.2.x86_64 .............................................[done]
(34/49) Removing libpurple-2.12.0-8.6.1.x86_64 ......................................................[done]
(35/49) Removing python-decorator-3.4.2-4.1.noarch ..................................................[done]
(36/49) Removing python-configobj-5.0.6-4.1.noarch ..................................................[done]
(37/49) Removing python-pyOpenSSL-16.0.0-1.1.noarch .................................................[done]
(38/49) Removing PackageKit-backend-zypp-1.1.3-2.3.x86_64 ...........................................[done]
(39/49) Removing python-solv-0.6.26-5.3.1.x86_64 ....................................................[done]
(40/49) Removing python-numpy-1.8.0-4.17.x86_64 .....................................................[done]
(41/49) Removing python-gobject2-2.28.6-27.5.x86_64 .................................................[done]
(42/49) Removing ibus-1.5.13-2.3.1.x86_64 ...........................................................[done]
(43/49) Removing libpurple-branding-openSUSE-42.2-3.3.2.noarch ......................................[done]
(44/49) Removing python-cryptography-1.3.1-2.2.x86_64 ...............................................[done]
(45/49) Removing PackageKit-1.1.3-2.3.x86_64 ........................................................[done]
Additional rpm output:
Unknown media type in type 'all/all'
Unknown media type in type 'all/allfiles'
(46/49) Removing dbus-1-python-1.2.0-7.34.x86_64 ....................................................[done]
(47/49) Removing python-six-1.9.0-6.2.noarch ........................................................[done]
(48/49) Removing python-gobject-3.20.1-3.2.x86_64 ...................................................[done]
(49/49) Removing python-2.7.13-25.3.1.x86_64 ........................................................[done]
```

- install python 2.7

refer to [BeginnersGuide/Download](https://wiki.python.org/moin/BeginnersGuide/Download)

`For Red Hat, install the python2 and python2-devel packages.`

```bash
sudo zypper install python2
Retrieving repository 'R-base' metadata .............................................................[done]
Building repository 'R-base' cache ..................................................................[done]
Loading repository data...
Reading installed packages...
'python2' not found in package names. Trying capabilities.
Resolving package dependencies...

The following 4 NEW packages are going to be installed:
  python python-gobject python-gobject-cairo python-gobject-Gdk

4 new packages to install.
Overall download size: 949.3 KiB. Already cached: 0 B. After the operation, additional 3.5 MiB will be used.
Continue? [y/n/...? shows all options] (y): y
Retrieving package python-2.7.13-25.3.1.x86_64                                                                 (1/4), 312.5 KiB (  1.4 MiB unpacked)
Retrieving: python-2.7.13-25.3.1.x86_64.rpm ...........................................[done (151.0 KiB/s)]
Retrieving package python-gobject-3.20.1-3.2.x86_64                                                            (2/4), 480.9 KiB (  1.9 MiB unpacked)
Retrieving: python-gobject-3.20.1-3.2.x86_64.rpm ......................................[done (529.5 KiB/s)]
Retrieving package python-gobject-cairo-3.20.1-3.2.x86_64                                                      (3/4),  65.2 KiB ( 14.3 KiB unpacked)
Retrieving: python-gobject-cairo-3.20.1-3.2.x86_64.rpm ................................[done (150.3 KiB/s)]
Retrieving package python-gobject-Gdk-3.20.1-3.2.x86_64                                                        (4/4),  90.7 KiB (149.5 KiB unpacked)
Retrieving: python-gobject-Gdk-3.20.1-3.2.x86_64.rpm ...................................[done (69.1 KiB/s)]
Checking for file conflicts: ........................................................................[done]
(1/4) Installing: python-2.7.13-25.3.1.x86_64 .......................................................[done]
(2/4) Installing: python-gobject-3.20.1-3.2.x86_64 ..................................................[done]
(3/4) Installing: python-gobject-cairo-3.20.1-3.2.x86_64 ............................................[done]
(4/4) Installing: python-gobject-Gdk-3.20.1-3.2.x86_64 ..............................................[done]
```

```bash
sudo zypper install python2-devel
Loading repository data...
Reading installed packages...
'python2-devel' not found in package names. Trying capabilities.
Resolving package dependencies...

The following NEW package is going to be installed:
  python-devel

1 new package to install.
Overall download size: 3.4 MiB. Already cached: 0 B. After the operation, additional 21.0 MiB will be used.
Continue? [y/n/...? shows all options] (y): y
Retrieving package python-devel-2.7.13-25.3.1.x86_64                                                           (1/1),   3.4 MiB ( 21.0 MiB unpacked)
Retrieving: python-devel-2.7.13-25.3.1.x86_64.rpm .....................................[done (591.4 KiB/s)]
Checking for file conflicts: ........................................................................[done]
(1/1) Installing: python-devel-2.7.13-25.3.1.x86_64 .................................................[done]
```

chengshen@linux-0ng9:~> sudo pip install biopython
root's password:
Collecting biopython
  Downloading biopython-1.70-cp27-cp27mu-manylinux1_x86_64.whl (2.2MB)
    100% |███████████████| 2.2MB 485kB/s
Requirement already satisfied: numpy in /usr/lib64/python2.7/site-packages (from biopython)
Installing collected packages: biopython
Successfully installed biopython-1.70