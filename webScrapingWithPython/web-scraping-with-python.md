# web scraping with python

## 建立本地站

open [web2pyTM Download](http://www.web2py.com/init/default/download)

click For Normal Users - For Windows

download web2py_win.zip

extract to C:\web2py

run `C:\web2py\web2py.exe`

enter password `1234`

click `start server`

install and configure mercurial

run powershell as administer

```Windows PowerShell
cd C:\web2py\applications
hg clone https://mapleaf@bitbucket.org/wswp/places
```

print

```Windows PowerShell
目标目录: places
中止: destination 'places' is not empty
PS C:\web2py\applications> hg clone https://mapleaf@bitbucket.org/wswp/places
目标目录: places
正在请求全部修改
正在增加修改集
正在增加清单
正在增加文件改变
已增加 2 个修改集，包含 361 个改变，修改了 355 个文件
updating to branch default
355 files updated, 0 files merged, 0 files removed, 0 files unresolved
```

run C:\web2py\web2py.exe as administer
open [loacl website](http://127.0.0.1:8000/places)

## CH1 网络爬虫简介

### 1.3 背景调研

#### 1.3.4 识别网站所用技术

install module `builtwith`

```Windows PowerShell
PS C:\Users\mapleaf_soar> py -2 -m pip install builtwith
You are using pip version 7.0.1, however version 9.0.1 is available.
You should consider upgrading via the 'pip install --upgrade pip' command.
Collecting builtwith
  Downloading builtwith-1.3.2.tar.gz
Installing collected packages: builtwith
  Running setup.py install for builtwith
Successfully installed builtwith-1.3.2
```

顺手更新pip

```Windows PowerShell
PS C:\Users\mapleaf_soar> py -2 -m pip install --upgrade pip
You are using pip version 7.0.1, however version 9.0.1 is available.
You should consider upgrading via the 'pip install --upgrade pip' command.
Collecting pip
  Downloading pip-9.0.1-py2.py3-none-any.whl (1.3MB)
    100% |████████████████████████████████| 1.3MB 123kB/s
Installing collected packages: pip
  Found existing installation: pip 7.0.1
    Uninstalling pip-7.0.1:
      Successfully uninstalled pip-7.0.1
Successfully installed pip-9.0.1
```

识别 example.webscraping.com 所用技术

```python
>>> builtwith.parse('http://example.webscraping.com')
{u'javascript-frameworks': [u'jQuery', u'Modernizr', u'jQuery UI'],
 u'web-frameworks': [u'Web2py', u'Twitter Bootstrap'],
 u'programming-languages': [u'Python'],
 u'web-servers': [u'Nginx']}
```

#### 1.3.5 寻找网站所有者

install module `python-whois`

```Windows PowerShell
PS C:\Users\mapleaf_soar> py -2 -m pip install python-whois
Collecting python-whois
  Downloading python-whois-0.6.5.tar.gz
Collecting future (from python-whois)
  Downloading future-0.16.0.tar.gz (824kB)
    100% |████████████████████████████████| 829kB 1.3MB/s
Installing collected packages: future, python-whois
  Running setup.py install for future ... done
  Running setup.py install for python-whois ... done
Successfully installed future-0.16.0 python-whois-0.6.5
```

use module `python-whois` to inquire jianshu

run python2 in powershell

```Windows PowerShell
PS C:\Users\mapleaf_soar> py -2
Python 2.7.10 (default, May 23 2015, 09:44:00) [MSC v.1500 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
```

inquire jianshu

```python
>>> import whois
>>> print whois .whois('http://www.jianshu.com')
{
  "updated_date": "2016-04-06 10:24:47",
  "status": [
    "clientTransferProhibited https://icann.org/epp#clientTransferProhibited",
    "clientTransferProhibited https://www.icann.org/epp#clientTransferProhibited"
  ],
  "name": "Shanghai Bai Ji Information Technology Inc. Ltd,",
  "dnssec": [
    "unsigned",
    "unSigned"
  ],
  "city": "Shanghai",
  "expiration_date": "2020-03-20 18:28:58",
  "zipcode": "200433",
  "domain_name": "JIANSHU.COM",
  "country": "CN",
  "whois_server": "whois.name.com",
  "state": "Shanghai",
  "registrar": "Name.com, Inc.",
  "referral_url": null,
  "address": "Innospace 2, B1, Building #5, KIC, No.316 Songhu Road , Yangpu District",
  "name_servers": [
    "F1G1NS1.DNSPOD.NET",
    "F1G1NS2.DNSPOD.NET",
    "f1g1ns1.dnspod.net",
    "f1g1ns2.dnspod.net"
  ],
  "org": "Shanghai Bai Ji Information Technology Inc. Ltd,",
  "creation_date": "2008-03-20 18:28:58",
  "emails": [
    "abuse@name.com",
    "contact@jianshu.com"
  ]
}
```

#### 1.4.1 下载网页

1. 重试下载

it is `None` and not `none` !!! 

the error object has an attribute named code!!!

```python
import urllib2
def download(url, num_retries=2):
    print 'Downloading:', url
    try:
        html = urllib2.urlopen(url).read()
    except urllib2.URLError as e:
        print 'Download error:', e.reason
        html = None
        if num_retries > 0:
            if hasattr(e, 'code') and 500 <= e.code < 600:
                # recursively retry 5xx HTTP errors
                print e.code
                return download(url, num_retries-1)
    return html

download('http://httpstat.us/500')
```

程序输出

```python
download('http://httpstat.us/500')
Downloading: http://httpstat.us/500
Download error: Internal Server Error
500
Downloading: http://httpstat.us/500
Download error: Internal Server Error
500
Downloading: http://httpstat.us/500
Download error: Internal Server Error
```

2. 设置用户代理（无法验证）

```python
import urllib2
def download(url, user_agent='wswp', num_retries=2):
    print 'Downloading:', url
    headers = {'User-agent': user_agent}
    request = urllib2.Request(url, headers=headers)
    try:
        html = urllib2.urlopen(request).read()
    except urllib2.URLError as e:
        print 'Download error:', e.reason
        html = None
        if num_retries > 0:
            if hasattr(e, 'code') and 500 <= e.code < 600:
                # recursively retry 5xx HTTP errors
                print e.code
                return download(url, num_retries-1)
    return html

```

#### 1.4.2 网站地图爬虫（sitemap有问题）

```python
def crawl_sitemap(url):
    # download the sitemap file
    sitemap = download(url)
    # extarct the sitemap links
    links = re.findall('<loc>(.*?)<loc>', sitemap)
    # download each link
    for link in links:
        html = download(link)
        # scrape html here
        # ...

import re
crawl_sitemap('http://example.webscraping.com/sitemap.xml')
```

#### 1.4.3 ID遍历爬虫

```python
import itertools
# maximum number of consecutive download errors allowed
max_errors = 5
# current number of consecutive download errors allowed
num_errors = 5
for page in itertools.count(1):
    url = 'http://example.webscraping.com/view/-%d' % page
    html = download(url)
    if html is None:
        # received an error trying to download this webpage
        num_errors += 1
        if num_errors == max_errors:
            # reached maximum number of
            # consecutive errors so exit
            break
    else:
        # success - can scrape the result
        # ...
        num_errors = 0
```

程序输出

```python
Downloading: http://example.webscraping.com/view/-1
Downloading: http://example.webscraping.com/view/-2
```

......

```python
Downloading: http://example.webscraping.com/view/-55
Downloading: http://example.webscraping.com/view/-56
Downloading: http://example.webscraping.com/view/-57
Download error: TOO MANY REQUESTS
Downloading: http://example.webscraping.com/view/-58
Download error: TOO MANY REQUESTS
Downloading: http://example.webscraping.com/view/-59
Download error: TOO MANY REQUESTS
Downloading: http://example.webscraping.com/view/-60
Download error: TOO MANY REQUESTS
Downloading: http://example.webscraping.com/view/-61
Download error: TOO MANY REQUESTS
```

