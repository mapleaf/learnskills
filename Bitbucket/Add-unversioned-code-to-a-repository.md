# Add unversioned code to a repository

**old location of the file `web scraping with python.md`**

run PowerShell

change to the root directory of existing code

```Windows PowerShell
PS C:\Users\mapleaf_soar> cd .\desktop
```

create a new folder named `webScrapingWithPython`

```Windows PowerShell
mkdir webScrapingWithPython
```

terminal output

```Windows PowerShell

    Directory: C:\Users\mapleaf_soar\desktop


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----        8/25/2017   2:56 PM                webScrapingWithPython

```

move the scripts named `web scraping with python.md` to the folder just created

```Windows PowerShell
mv '.\web scraping with python.md' '.\webScrapingWithPython\web scraping with python.md'
```

change to the folder named `webScrapingWithPython`

```Windows PowerShell
# enter 'cd .\', press tab several times to autocomplete the floder name
PS C:\Users\mapleaf_soar\desktop> cd .\webScrapingWithPython\
```

initialize the directory under source control

```Windows PowerShell
PS C:\Users\mapleaf_soar\desktop\webScrapingWithPython> git init
```

terminal output

```Windows PowerShell
Initialized empty Git repository in C:/Users/mapleaf_soar/Desktop/webScrapingWithPython/.git/
```

Add the existing files to the repository just initialized (no output)

```Windows PowerShell
git add .
```

Commit the files

```Windows PowerShell
git commit -m 'initial commit of full repository'
```

terminal output

```Windows PowerShell
[master (root-commit) 3342e87] initial commit of full repository
 1 file changed, 288 insertions(+)
 create mode 100644 web scraping with python.md
```

From [the Overview page of the repository just created in Bitbucket], choose I have an existing project.

Connect local repository to the remote repository on Bitbucket. To do that, copy the git remote path under "I have an existing project" and enter it in the command line.

If can't find the web page, git remote path following command `git remote add origin` is like `https://<username_on_Bitbucket>@bitbucket.org/<username_on_Bitbucket>/<name_of_repository>.git` or `git@bitbucket.org:<username_on_Bitbucket>/<name_of_repository>.git`

```Windows PowerShell
git remote add origin https://mapleaf@bitbucket.org/mapleaf/learnskills.git
```

**another** **two useful command**

1. remove `git remote remove <name>`

2. show remote

```Windows PowerShell
git remote -v
origin  https://mapleaf@bitbucket.org/mapleaf/learnskills.git (fetch)
origin  https://mapleaf@bitbucket.org/mapleaf/learnskills.git (push)
```

Push all the code in local repository to Bitbucket

```Windows PowerShell
PS C:\Users\mapleaf_soar\desktop\webScrapingWithPython> git push -u origin --all
```

terminal output !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

```Windows PowerShell
To https://bitbucket.org/mapleaf/learnskills.git
 ! [rejected]        master -> master (fetch first)
error: failed to push some refs to 'https://mapleaf@bitbucket.org/mapleaf/learnskills.git'
hint: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
hint: to the same ref. You may want to first integrate the remote changes
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

attention to hint

`because the remote contains work that you do
not have locally. This is usually caused by another repository pushing to the same ref. You may want to first integrate the remote changes (e.g., 'git pull ...') before pushing again.`

because i have just commited `README.md` and `.gitignore`

so change the directory and fetch the repo first

```Windows PowerShell
PS C:\Users\mapleaf_soar\desktop\webScrapingWithPython> cd ..\
PS C:\Users\mapleaf_soar\Desktop> git clone git@bitbucket.org:mapleaf/learnskills.git
```

terminal output

```Windows PowerShell
Cloning into 'learnskills'...
remote: Counting objects: 6, done.
remote: Compressing objects: 100% (5/5), done.
remote: Total 6 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (6/6), done.
```

check remote

```Windows PowerShell
PS C:\Users\mapleaf_soar\Desktop\learnskills> git remote -v
```

terminal output

```Windows PowerShell
origin  git@bitbucket.org:mapleaf/learnskills.git (fetch)
origin  git@bitbucket.org:mapleaf/learnskills.git (push)
```

move floder `webScrapingWithPython` into folder `learnskills`

```Windows PowerShell
PS C:\Users\mapleaf_soar\Desktop\learnskills> mv '..\webScrapingWithPython\' '.\'
```

add floder `webScrapingWithPython` to local repo

```Windows PowerShell
PS C:\Users\mapleaf_soar\Desktop\learnskills> git add .\webScrapingWithPython
```

commit floder `webScrapingWithPython`

```Windows PowerShell
PS C:\Users\mapleaf_soar\Desktop\learnskills> git commit -m 'add floder webScrapingWithPython'
[master 39ebb8e] add floder webScrapingWithPython
 1 file changed, 288 insertions(+)
 create mode 100644 webScrapingWithPython/web scraping with python.md
```

Push all the code in local repository to Bitbucket ***again***

```Windows PowerShell
PS C:\Users\mapleaf_soar\Desktop\learnskills> git push -u origin --all
```

terminal output

```Windows PowerShell
Branch master set up to track remote branch master from origin.
Everything up-to-date
```