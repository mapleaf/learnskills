# daily learning

## man

useful command `man` with other command want to know

## check server equitment

- check Linux distribution by `cat /etc/issue | grep Linux`
- check CPU info by `cat /proc/cpuinfo | grep name | cut -f2 -d: | uniq -c`
- check memory by `cat /proc/meminfo` and `free -m`

## change password by `passwd`

```bash
更改用户 shenc 的密码 。
为 shenc 更改 STRESS 密码。
（当前）UNIX 密码：
新的 密码：
重新输入新的 密码：
passwd： 所有的身份验证令牌已经成功更新。
```

## install and use command `tree`

- download source code

```bash
wget http://mama.indstate.edu/users/ice/tree/src/tree-1.7.0.tgz
```

- unpack the tarball and change directory

```bash
tar zxvf tree-1.7.0.tgz

cd ./tree-1.7.0
```

- complie by `make`

- error reported when install

```bash
make install

install -d /usr/bin
install: 无法更改"/usr/bin" 的权限: 不允许的操作
make: *** [install] 错误 1
```

- check `Makefile` by `vim Makefile`
- noticed code below and info `install: 无法更改"/usr/bin" 的权限: 不允许的操作` above

```bash
prefix = /usr

CC=gcc

VERSION=1.7.0
TREE_DEST=tree
BINDIR=${prefix}/bin
```

- modify user location by changing `prefix = /usr` to `prefix = /home/pengb`
- install again and success

```bash
make install

install -d /home/pengb/bin
install -d /home/pengb/man/man1
if [ -e tree ]; then \
                install tree /home/pengb/bin/tree; \
        fi
install doc/tree.1 /home/pengb/man/man1/tree.1
```

## openSUSE SSH server configure

refer to [this](https://opensuse-guide.ustclug.org/srvother.php)

- enable SSH service

```bash
sudo systemctl enable sshd.service

Created symlink from /etc/systemd/system/multi-user.target.wants/sshd.service to /usr/lib/systemd/system/sshd.service.

sudo systemctl start sshd.service
```

- open firewall port

`Yast` - `secure and user` - `firewall` - `service allowed` - add  `SSH Server` - `apply`

## check ip address

type `ip addr show` and ip address will be shown fellowed by `inet`

## command SSH(Secure Shell)

- 远程访问工具
- 客户端发起TCP连接，与服务端协商双方共同使用的协议版本、身份认证算法、加密算法，建立起安全的通信通道。
- 加密传输数据，不易受攻击 | 数据经过压缩，传输速度较快

### Linux 主机间的 SSH 连接

```bash
ssh [-1246AaCfGgKkMNnqsTtVvXxYy] [-b bind_address] [-c cipher_spec]
    [-D [bind_address:]port] [-E log_file] [-e escape_char]
    [-F configfile] [-I pkcs11] [-i identity_file] [-L address]
    [-l login_name] [-m mac_spec] [-O ctl_cmd] [-o option] [-p port]
    [-Q query_option] [-R address] [-S ctl_path] [-W host:port]
    [-w local_tun[:remote_tun]] [user@]hostname [command]
```

`ssh <user_name>@<hostname|ip_address>`

`ssh shenc@192.168.41.175` or `ssh -l shenc 192.168.41.175`

- the first time login the server, info below will be shown

```bash
The authenticity of host '<ip_address> (<ip_address>)' can't be established.
RSA key fingerprint is SHA256:z4i+YYupuvd..........VuZHLcXaLRVBw.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '<ip_address>' (RSA) to the list of known hosts.
```

- type required password and login

```bash
<user_name>@<ip_address>'s password:
Last login: <time_last_login> from <client_ip_address>
```

type `exit` to logout and info below will be shown

```bash
logout
Connection to <hostname> closed.
```

### login SSH remote by trust relationship without password

refer to [this](http://blog.csdn.net/u014675548/article/details/51915380)

#### create an SSH trust relationship (chengshen --> shenc)

- from chengshen@192.168.190.173 to shenc@192.168.41.175

  - on local remove existing SSH key by `rm -r $HOME/.ssh/`
  - on local generate SSH key again by `ssh-keygen -t rsa`
    - choose the default location to store the key
    - set passphrase empty or password is still needed after create an trust relationship
    ```bash
    Generating public/private rsa key pair.
    Enter file in which to save the key (/home/chengshen/.ssh/id_rsa):
    Created directory '/home/chengshen/.ssh'.
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    Your identification has been saved in /home/chengshen/.ssh/id_rsa.
    Your public key has been saved in /home/chengshen/.ssh/id_rsa.pub.
    The key fingerprint is:
    SHA256:snhgfO8W18jOvD9+8zZAuT4/f6n9t1z10X+eb2ofty0 chengshen@linux-0ng9
    The key's randomart image is:
    +---[RSA 2048]----+
    |                 |
    |                 |
    |              .  |
    |   .         o  .|
    |    + o S. o. ..o|
    |   . + +. + .o  =|
    |    . o .*  . ..B|
    |     . .. + .oE=&|
    |       .. .+o==/^|
    +----[SHA256]-----+
    ```
  - transfer local public key to remote meanwhile modify file `authorized_keys` of remote

  ```bash
  chengshen@linux-0ng9:~> scp ./.ssh/id_rsa.pub shenc@192.168.41.175:/home/shenc/.ssh/authorized_keys
  shenc@192.168.41.175's password:
  id_rsa.pub                       100%  402     0.4KB/s   00:00
  ```

  - if `authorized_keys` is existing and has content in it
    - on local transfer the public key to remote
    ```bash
    scp ./.ssh/id_rsa.pub shenc@192.168.41.175:/home/shenc/.ssh/id_rsa.pub.from.chengshen
    ```
    - on remote add new public key into `authorized_keys`
    ```bash
    cat /home/shenc/.ssh/id_rsa.pub.from.chengshen >> /home/shenc/.ssh/authorized_key
    ```
  - could login without password after create trust relationship
  ```bash
  chengshen@linux-0ng9:~> ssh shenc@192.168.41.175
  Last login: Wed Dec  6 21:44:56 2017 from 192.168.190.173
  ```

#### create an SSH trust relationship (shenc --> chengshen)

```bash
[shenc@host025 ~]$ rm -r ./.ssh/

[shenc@host025 ~]$ ssh-keygen

[shenc@host025 ~]$ scp ./.ssh/id_rsa.pub chengshen@192.168.190.173:/home/chengshen/.ssh/authorized_keys

[shenc@host025 ~]$ ssh chengshen@192.168.190.173
Last login: Wed Dec  6 22:51:08 2017 from 192.168.41.175
Have a lot of fun...
```

## a try of transfer file to remote

- recall download of iGenome with command below

```bash
wget ftp://igenome:G3nom3s4u@ussd-ftp.illumina.com/Drosophila_melanogaster/Ensembl/BDGP6/Drosophila_melanogaster_Ensembl_BDGP6.tar.gz

--2017-12-05 23:36:21--  ftp://igenome:*password*@ussd-ftp.illumina.com/Drosophila_melanogaster/Ensembl/BDGP6/Drosophila_melanogaster_Ensembl_BDGP6.tar.gz
           => “Drosophila_melanogaster_Ensembl_BDGP6.tar.gz”
正在解析主机 ussd-ftp.illumina.com (ussd-ftp.illumina.com)... 66.192.10.36
正在连接 ussd-ftp.illumina.com (ussd-ftp.illumina.com)|66.192.10.36|:21... 已连接。
正在以 igenome 登录 ... 登录成功！
==> SYST ... 完成。   ==> PWD ... 完成。
==> TYPE I ... 完成。 ==> CWD (1) /Drosophila_melanogaster/Ensembl/BDGP6 ... 完成。
==> SIZE Drosophila_melanogaster_Ensembl_BDGP6.tar.gz ... 762893718
==> PASV ...
```

- noticed username and passwd in FTP URL and output

```bash
ftp://igenome:G3nom3s4u@ussd-ftp.illumina.com

正在以 igenome 登录 ... 登录成功！
```

- use the same pattern to try transfer file and unsuccess

```bash
wget ftp://pengb:pengb@192.168.41.176/home/pengb/Data/RawData/1DG2_D_rep1
--2017-12-05 23:38:03--  ftp://pengb:*password*@192.168.41.176/home/pengb/Data/RawData/1DG2_D_rep1
           => “1DG2_D_rep1”
正在连接 192.168.41.176:21... 失败：没有到主机的路由。

wget ftp://pengb:pengb@ftp.192.168.41.176/home/pengb/Data/RawData/1DG2_D_rep1
--2017-12-05 23:38:37--  ftp://pengb:*password*@ftp.192.168.41.176/home/pengb/Data/RawData/1DG2_D_rep1
           => “1DG2_D_rep1”
正在解析主机 ftp.192.168.41.176 (ftp.192.168.41.176)... 失败：未知的名称或服务。
wget: 无法解析主机地址 “ftp.192.168.41.176”

wget ftp://pengb:pengb@host026/home/pengb/Data/RawData/1DG2_D_rep1
--2017-12-05 23:39:07--  ftp://pengb:*password*@host026/home/pengb/Data/RawData/1DG2_D_rep1
           => “1DG2_D_rep1”
正在解析主机 host026 (host026)... 失败：未知的名称或服务。
wget: 无法解析主机地址 “host026”
```

- look for other remote file transfer method

- suppose that the remote donot install FTP server

## command SCP(Secure copy)

- Remote file(support rename)/folder(use `-r` parameter) encrypted transfer
- require remote has install SSH server, refer to `openSUSE SSH server configure`
- regard SCP as command `cp` with SSH protocol

```bash
scp [-12346BCpqrv] [-c cipher] [-F ssh_config] [-i identity_file]
    [-l limit] [-o ssh_option] [-P port] [-S program]
    [[user@]host1:]file1 ... [[user@]host2:]file2
```

### transfer without password

after create a trust relation refer to section above

## command [ctrl-z]

[shenc@host025 RawData]$ scp -r /home/shenc/data/RawData/ chengshen@192.168.190.173:/home/chengshen
Password:
HepG2_rep1.2_2_fastqc.zip                                                       100%  811KB 810.6KB/s   00:00
HepG2_rep1.a.fq.gz                                                               20%  277MB   3.9MB/s   04:29 ETA^Z
[1]+  Stopped                 scp -r /home/shenc/data/RawData/ chengshen@192.168.190.173:/home/chengshen

## command `jobs`

[shenc@host025 RawData]$ jobs
[1]+  Stopped                 scp -r /home/shenc/data/RawData/ chengshen@192.168.190.173:/home/chengshen

## command `bg`

[shenc@host025 RawData]$ bg %1
[1]+ scp -r /home/shenc/data/RawData/ chengshen@192.168.190.173:/home/chengshen &

## command

[shenc@host025 RawData]$ jobs
[1]+  Running                 scp -r /home/shenc/data/RawData/ chengshen@192.168.190.173:/home/chengshen &

## command `top`

```bash
top - 18:21:22 up  3:00,  4 users,  load average: 0.16, 0.09, 0.02
Tasks: 321 total,   1 running, 320 sleeping,   0 stopped,   0 zombie
Cpu(s):  2.9%us,  0.6%sy,  0.0%ni, 96.4%id,  0.1%wa,  0.0%hi,  0.0%si,  0.0%st
Mem:  24390376k total,  3009648k used, 21380728k free,   207116k buffers
Swap: 10207224k total,        0k used, 10207224k free,   640624k cached

  PID USER  PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND
 3813 root  20   0 1257m 544m  32m S 23.9  2.3  39:34.95 firefox
 4340 root  20   0 2191m 161m  42m S  4.3  0.7   5:06.45 rstudio
 2619 root  20   0  231m 162m  26m S  0.7  0.7   0:49.26 Xvnc
 2426 root  20   0  692m  55m  27m S  0.3  0.2   0:01.69 hostd-worker
 3219 root  20   0  287m  16m  11m S  0.3  0.1   0:39.46 vino-server
    1 root  20   0 19244 1424 1156 S  0.0  0.0   0:00.92 init
    2 root  20   0     0    0    0 S  0.0  0.0   0:00.00 kthreadd
    3 root  RT   0     0    0    0 S  0.0  0.0   0:00.00 migration/0
    4 root  20   0     0    0    0 S  0.0  0.0   0:00.00 ksoftirqd/0
    5 root  RT   0     0    0    0 S  0.0  0.0   0:00.00 watchdog/0
    6 root  RT   0     0    0    0 S  0.0  0.0   0:00.00 migration/1
    7 root  20   0     0    0    0 S  0.0  0.0   0:00.01 ksoftirqd/1
    8 root  RT   0     0    0    0 S  0.0  0.0   0:00.00 watchdog/1
    9 root  RT   0     0    0    0 S  0.0  0.0   0:00.00 migration/2
   10 root  20   0     0    0    0 S  0.0  0.0   0:00.00 ksoftirqd/2
   11 root  RT   0     0    0    0 S  0.0  0.0   0:00.00 watchdog/2
   12 root  RT   0     0    0    0 S  0.0  0.0   0:00.00 migration/3
   13 root  20   0     0    0    0 S  0.0  0.0   0:00.00 ksoftirqd/3
   14 root  RT   0     0    0    0 S  0.0  0.0   0:00.00 watchdog/3
   15 root  RT   0     0    0    0 S  0.0  0.0   0:00.00 migration/4
   16 root  20   0     0    0    0 S  0.0  0.0   0:00.00 ksoftirqd/4
   17 root  RT   0     0    0    0 S  0.0  0.0   0:00.00 watchdog/4
   18 root  RT   0     0    0    0 S  0.0  0.0   0:00.00 migration/5
   19 root  20   0     0    0    0 S  0.0  0.0   0:00.00 ksoftirqd/5
   20 root  RT   0     0    0    0 S  0.0  0.0   0:00.00 watchdog/5
   21 root  RT   0     0    0    0 S  0.0  0.0   0:00.00 migration/6
   22 root  20   0     0    0    0 S  0.0  0.0   0:00.00 ksoftirqd/6
   23 root  RT   0     0    0    0 S  0.0  0.0   0:00.00 watchdog/6
   24 root  RT   0     0    0    0 S  0.0  0.0   0:00.00 migration/7
   25 root  20   0     0    0    0 S  0.0  0.0   0:00.00 ksoftirqd/7
   26 root  RT   0     0    0    0 S  0.0  0.0   0:00.00 watchdog/7
   27 root  20   0     0    0    0 S  0.0  0.0   0:00.00 events/0
   28 root  20   0     0    0    0 S  0.0  0.0   0:02.87 events/1
   29 root  20   0     0    0    0 S  0.0  0.0   0:00.01 events/2
   30 root  20   0     0    0    0 S  0.0  0.0   0:00.16 events/3
   31 root  20   0     0    0    0 S  0.0  0.0   0:00.00 events/4
   32 root  20   0     0    0    0 S  0.0  0.0   0:00.00 events/5
   33 root  20   0     0    0    0 S  0.0  0.0   0:00.00 events/6
   34 root  20   0     0    0    0 S  0.0  0.0   0:00.00 events/7
   35 root  20   0     0    0    0 S  0.0  0.0   0:00.00 cpuset
```

## command `wget`

- download all files in dir `packages` from `http://example.com`, `-np` do not raversal parent dir, `-nd` donot create dir structure on local

```bash
wget -r -np -nd http://example.com/packages/
```

choose the format of files to download

```bash
wget -r --accept=R,sh ftp://ftp.ccb.jhu.edu/pub/RNAseq_protocol/
```

## command `echo`

- output <str> to <file> ( `>` represents overriding original file)

```bash
echo "<str>" > <file>
```

- output <str> to <file> ( `>>` represents appending the output content to original file)

```bash
echo "<str>" >> <file>
```