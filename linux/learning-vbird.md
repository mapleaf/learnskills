# 鸟哥的Linux私房菜 基础篇

## CH3 Install CentOS 7.X

### 3.2 开始安装CentOS 7

#### 3.2.1 调整开机媒体(BIOS)与虚拟机建置流程

install Virtualbox, click `新建`, click `专家模式`

```option
虚拟电脑名称和系统类型
名称 CentOS-7-x86_64-Everything-1611
类型(Linux)/版本(Red Hat(64-bit)) 自动选择

内存大小
1024MB

虚拟硬盘
现在创建虚拟硬盘
```

click `创建`

```option
文件位置
D:\Development\VirtualBox VMs\CentOS-7-x86_64-Everything-1611\CentOS-7-x86_64-Everything-1611.vdi

文件大小
40GB

虚拟硬盘文件类型
VDI

存储在物理硬盘上
固定大小
```

click `创建`

```dialog box shows
创建虚拟硬盘: Creating fixed medium storage unit 'D:\Development\VirtualBox VMs\CentOS-7-x86_64-Everything-1611\CentOS-7-x86_64-Everything-1611.vdi'...
进度条
剩余时间：<>分钟
```

wait for finish, click `设置`

click `系统`-`主板`

```option
启动顺序
check 光驱 box

指点设备
USB触控板
```

click `存储`

```option
存储树-控制器, click '没有盘片'

属性-分配光驱
第二IDE控制器主通道
click CD symbol to choose .iso file
```

click `OK`

click `启动`

enter the interface of installing CentOS

press `up`/`down` to point `Install CentOS Linux 7`

press `Tab`

enter a space character and `inst.gpt`

show some messages like `[ OK ] Started ...`

choose language

root passoword `12345` / user `mapleaf` password `1234`

## CH5 首次登录与在线求助 man page

### 5.1 首次登录系统

- Linux 是多人多工的环境
- 用户 `username` 的个人主文件夹在 `/home/<username>` ,即 `~` 下
- `~` of root is `/root`
- GNOME 的文件资源管理器称为 `Nautilus/鹦鹉螺`; KDE~`Konqueror/征服家`
- host/terminal/console/tty

### 5.2 在命令行模式下执行命令

- command [-optionals] parameter1 parameter2...
- use `-o` or `--optionals` in shell command
- use `\[Enter]` to continue to type command in next line
- Linux is sensitive to case
- `locale` / description of multilanguage support / A locale is a set of language and cultural rules.

```bash
[username@hostname ~]$ locale
LANG=zh_CN.UTF-8
LC_CTYPE="zh_CN.UTF-8"
LC_NUMERIC="zh_CN.UTF-8"
LC_TIME="zh_CN.UTF-8"
LC_COLLATE="zh_CN.UTF-8"
LC_MONETARY="zh_CN.UTF-8"
LC_MESSAGES="zh_CN.UTF-8"
LC_PAPER="zh_CN.UTF-8"
LC_NAME="zh_CN.UTF-8"
LC_ADDRESS="zh_CN.UTF-8"
LC_TELEPHONE="zh_CN.UTF-8"
LC_MEASUREMENT="zh_CN.UTF-8"
LC_IDENTIFICATION="zh_CN.UTF-8"
LC_ALL=
```

- use `echo $LANG` and `LANG=en_US` to view and change the language of output
- use `expotr LC_ALL=en_US.utf8` to change all the language
- change is effective in the current login
- use `date --help` to view date and time

```bash
date +%Y/%m/%d[%H:%M:%S]
2017/10/04[16:14:43]
```

- use `cal -h` to view calendar
- use `bc` to run simple calculator; use `scale=<number>` to change bits of number
- execute command in terminal ***and*** 
    - command show output ***then*** return to command prompt ***or***
    - enter the environment of the command ***then*** return to command prompt ***until*** finish the command
- use `[tab]` to complete name of command, option, parameter or file with once or twice
- use `[Ctrl]-c` to terminate the current command
- use `[Ctrl]-d` to exit the terminal(End Of File, EOF)
- use `[Shift]+{[PgUp]|[PgDn]}` to flip pages in terminal
- pay attention to error message in terminal

### 5.3 Linux 系统的在线求助 man page 与 info page

- 
